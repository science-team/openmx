#
# File Name
#

System.CurrrentDirectory         ./    # default=./
System.Name                      fe
level.of.stdout                   1    # default=1 (1-3)
level.of.fileout                  1    # default=1 (1-3)

#
# flag of memory leak checker
#

memory.leak                      on    # on|off, default=off

#
# Definition of Atomic Species
#

Species.Number       1
<Definition.of.Atomic.Species
 Fe  Fe6.0S-s2p1d1       Fe_PBE13S
Definition.of.Atomic.Species>

#
# Atoms
#

Atoms.Number         2
Atoms.SpeciesAndCoordinates.Unit   Ang  # Ang|AU

<Atoms.SpeciesAndCoordinates           
 1  Fe  0.000     0.000     0.000       8.0 6.0
 2  Fe  1.2350    1.4350    1.4350     8.0 6.0
Atoms.SpeciesAndCoordinates>

Atoms.UnitVectors.Unit             Ang # Ang|AU
<Atoms.UnitVectors                     
   2.8700   0.0000   0.0000
   0.0000   2.8700   0.0000
   0.0000   0.0000   2.8700
Atoms.UnitVectors>

#
# SCF or Electronic System
#

scf.XcType                 GGA-PBE     # LDA|LSDA-CA|LSDA-PW
scf.SpinPolarization        on         # On|Off
scf.partialCoreCorrection   On         # On|Off
scf.ElectronicTemperature  300.0       # default=300 (K)
scf.energycutoff           120.0       # default=150 (Ry)
scf.maxIter                  2         # default=40
scf.EigenvalueSolver      band         # Recursion|Cluster|Band
scf.Kgrid                 3 2 1        # means n1 x n2 x n3
scf.Mixing.Type            rmm-diisk   # Simple|Rmm-Diis|Gr-Pulay
scf.Init.Mixing.Weight     0.30        # default=0.30 
scf.Min.Mixing.Weight      0.001       # default=0.001 
scf.Max.Mixing.Weight      0.400       # default=0.40 
scf.Mixing.History          7          # default=5
scf.Mixing.StartPulay      13          # default=6
scf.criterion             1.0e-9       # default=1.0e-6 (Hartree) 

#
# 1D FFT
#

1DFFT.NumGridK             900         # default=900
1DFFT.NumGridR             900         # default=900
1DFFT.EnergyCutoff        3600.0       # default=3DFFT.EnergyCutoff*3.0 (Ry)

#
# Orbital Optimization
#

orbitalOpt.Method           off        # Off|Unrestricted|Restricted
orbitalOpt.InitCoes     Symmetrical    # Symmetrical|Free
orbitalOpt.initPrefactor   0.1         # default=0.1
orbitalOpt.scf.maxIter      30         # default=12
orbitalOpt.MD.maxIter       20         # default=5
orbitalOpt.per.MDIter        2         # default=1000000
orbitalOpt.criterion      1.0e-4       # default=1.0e-4 (Hartree/borh)^2

#
# output of contracted orbitals
#

CntOrb.fileout               off       # on|off, default=off
Num.CntOrb.Atoms              1        # default=1
<Atoms.Cont.Orbitals
 1
Atoms.Cont.Orbitals>
 
#
# SCF Order-N
#

orderN.HoppingRanges        9.2        # default=5.0 (Ang) 
orderN.NumHoppings           3         # default=2
orderN.KrylovH.order        600        # default=400
orderN.recalc.EM            off

#
# restart using *.rst
#

scf.restart                  off       # on|off, default=off


#
# MD or Geometry Optimization
#

MD.Type                     opt        # Nomd|Constant_Energy_MD|Opt
MD.maxIter                   20        # default=1
MD.TimeStep                   1        # default=0.5 (fs)
MD.Opt.criterion         1.0e-5        # default=1.0e-4 (Hartree/bohr)

#
# Band dispersion 
#

Band.dispersion              off       # on|off, default=off
# if <Band.KPath.UnitCell does not exist,
#     the reciprical lattice vector is employed. 
Band.Nkpath                5
<Band.kpath                
   15  0.0 0.0 0.0   1.0 0.0 0.0   g X
   15  1.0 0.0 0.0   1.0 0.5 0.0   X W
   15  1.0 0.5 0.0   0.5 0.5 0.5   W L
   15  0.5 0.5 0.5   0.0 0.0 0.0   L g
   15  0.0 0.0 0.0   1.0 1.0 0.0   g X 
Band.kpath>

#
# MO output
#

MO.fileout                       off   # on|off
num.HOMOs                         1    # default=2
num.LUMOs                         1    # default=2

MO.Nkpoint                        1    # default=1 
<MO.kpoint
  0.0  0.0  0.0
MO.kpoint>

#
# DOS and PDOS
#

Dos.fileout                  off       # on|off, default=off
Dos.Erange              -20.0  20.0    # default = -20 20 
Dos.Kgrid                12 12 12      # default = Kgrid1 Kgrid2 Kgrid3


HS.fileout                   off       # on|off, default=off

